export type HelpArg = {
    required: boolean,
    name: string
};

export type HelpLine = {
    id: string,
    type: 'cmd' | 'route',
    description: string,
    args: HelpArg[]
};

export type HelpMessage = {
    title: string,
    description: string,
    lines: HelpLine[]
};