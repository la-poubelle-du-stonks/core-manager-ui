import "./prototype.scss";

import { CockpitClient } from "../../cockpit/Cockpit";
import { Widget } from "../widget";
import { HelpArg, HelpLine, HelpMessage } from "./helpTypes";
import { ShellForm, TextInput } from "../../ui/form";
import { TiledLayout } from "../../ui/tiled/tiledLayout";

export class ExplorerPrototypeWidget implements Widget {

    self: HTMLDivElement = document.createElement("div");
    path: string[] = [];

    private _loading = false;
    private h: HTMLHeadingElement;
    private nav: HTMLDivElement;

    constructor (private client: CockpitClient, private layout: TiledLayout) {

        this.self.className = "explorer-proto";
        this.h = document.createElement("h1");
        this.nav = document.createElement("div");
        this.nav.className = "nav";
        
        this.navigate();

    }

    mount (parent: Node): void {

        parent.appendChild(this.self);

    }

    unmount (): void {

    }

    async back () {
        this.path.pop();
        await this.navigate();
    }

    async navigate (...path: string[]) {

        this.path = [ ...this.path, ...path ];
        let command = [ ...this.path, "help" ].join(" ");

        this.loading = true;

        let data = await this.client.request(command);

        if (data.type != "help") {
            this.client.error("Bad packet routage");
            this.loading = false;
            return;
        }

        // clears the explorer
        this.self.innerHTML = "";

        let help = <HelpMessage>JSON.parse(data.payload);

        this.nav.innerText = ["", ...this.path, ""].join(" / ");
        this.self.append(this.nav);

        this.h.innerText = help.title;
        if (help.description) this.h.innerText += " - " + help.description;
        this.self.append(this.h);

        let ul = document.createElement("ul");

        let mapArg = (a: HelpArg) => a.required ? `<${a.name}>` : `[${a.name}]`;
        let sortList = (a: HelpLine, b: HelpLine) => a.id < b.id ? -1 : 1;

        let routes = help.lines.filter(e => e.type == "route").sort(sortList);
        let cmds   = help.lines.filter(e => e.type == "cmd").sort(sortList);

        if (this.path.length) {
            let li = document.createElement("li");
            li.innerText = "..";
            li.onclick = () => this.back();
            ul.append(li);
        }

        for (let r of routes) {
            let li = document.createElement("li");

            li.innerText = `[${r.type}] ${r.id} : ${r.description}`;
            li.onclick = () => this.navigate(r.id);

            ul.append(li);
        }

        for (let c of cmds) {
            let li = document.createElement("li");

            li.innerText = `[${c.type}] ${[c.id, ...c.args.map(mapArg)].join(" ")} : ${c.description}`;
            li.onclick = () => {
                let cmd = [...this.path, c.id].join(" ");
                if (!c.args.length) {
                    // No args required
                    this.client.wildCommand(cmd);
                } else {
                    // Request user some args and then execute the command
                    this.commandRequest(cmd, c);
                }
            }

            ul.append(li);
        }

        this.self.append(ul);

    }

    private set loading (l: boolean) {
        this._loading = l;
        this.self.classList.toggle("loading", l);
    }

    get loading () {
        return this._loading;
    }

    commandRequest (command: string, help: HelpLine) {

        let form = new ShellForm();
        
        // Adding the buttons
        let ok = form.addButton("OK");
        let addArg = form.addButton("Add Argument")
        let cancel = form.addButton("Cancel");
        
        let inputs: TextInput[] = [];
        
        // Function to execute once the form is complete
        let validate = () => {
            let str = [
                command,
                ...inputs
                    .filter(e => e.input.value != "")
                    .map(e => JSON.stringify(e.input.value))
            ].join(" ");
            this.client.wildCommand(str);
            this.layout.popup.close();
        }

        // Function that adds an input to the form
        let addInput = (arg: HelpArg) => {
            let i = form.addInput(
                arg.name.toLowerCase(),
                arg.name != "" 
                    && (arg.required ? `<${arg.name}>` : `[${arg.name}]`) 
                    || undefined
            );
            i.input.onkeyup = e => {
                if (e.key == "Enter") {
                    validate();
                }
            }
            inputs.push(i);
        }
        
        // Binding the buttons actions
        ok.onclick = () => validate();
        cancel.onclick = () => this.layout.popup.close();
        addArg.onclick = () => addInput({ name: "", required: false });
    
        // Adding a description
        form.addTitle("> " + command);
        form.addText(help.description);

        // Adding the argument inputs 
        for (let arg of help.args) {
            addInput(arg);
        }
        
        // Showing the popup
        this.layout.popup.open(form.self);
    
    }

}
