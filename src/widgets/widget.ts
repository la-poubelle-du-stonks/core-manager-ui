export interface Widget {

    self: HTMLDivElement;
    mount: (parent: Node) => void;
    unmount: () => void;

}