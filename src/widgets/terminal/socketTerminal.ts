import { Readline } from "xterm-readline";
import { TerminalWidget } from "./terminal";

export class SocketTerminalWidget extends TerminalWidget {

    sock: WebSocket;
    rl: Readline;

    constructor (url: string) {
        
        super();

        this.sock = new WebSocket(url);

        this.sock.onclose = () => {
            this.xterm.writeln("\x1b[31m$ Socket closed $\x1b[0m");
        }

        this.sock.onerror = () => {
            this.xterm.writeln("\x1b[31m$ Socket error $\x1b[0m");
        }

        this.sock.onopen = () => {
            this.xterm.writeln(`\x1b[32m$ Socket opened at ${url} $\x1b[0m`);
            this.sock.send("@raw\0");
        }

        this.sock.onmessage = msg => {
            this.xterm.write(msg.data);
        }


        this.rl = new Readline();
        this.xterm.loadAddon(this.rl);

    }

    mount(parent: Node): void {
        super.mount(parent);
        this.read();
    }

    private read () {
        this.rl.read("").then(data => this.send(data));
    }

    private send (data: string) {
        this.sock.send(data);
        if (this.mounted)
            setTimeout(() => this.read());
    }

}