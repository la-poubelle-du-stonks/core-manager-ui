import { Readline } from "xterm-readline";
import { TerminalLogger, TerminalWidget } from "./terminal";
import { CockpitClient, Packet } from "../../cockpit/Cockpit";
import { PrettyPrint } from "../../cockpit/prettyprint";

export class CockpitTerminalWidget extends TerminalWidget {

    sock: WebSocket;
    rl: Readline;
    cockpit: CockpitClient;
    logger: TerminalLogger;
    pretty: PrettyPrint;

    constructor (url: string) {
        
        super();

        this.logger = new TerminalLogger(this.xterm);
        this.sock = new WebSocket(url);
        this.pretty = new PrettyPrint();

        this.cockpit = new CockpitClient(
            "web_ui",
            msg => this.sock.send(JSON.stringify(msg) + "\0"),
            this.logger,
            this.pretty
        );

        this.sock.onclose = () => {
            this.xterm.writeln("\x1b[31m$ Socket closed $\x1b[0m");
        }

        this.sock.onerror = () => {
            this.xterm.writeln("\x1b[31m$ Socket error $\x1b[0m");
        }

        this.sock.onopen = () => {
            this.xterm.writeln(`\x1b[32m$ Socket opened at ${url} $\x1b[0m`);
            this.sock.send("@json\0");
        }

        this.sock.onmessage = msg => {
            // this.xterm.write(msg.data);
            this.cockpit.recieve(JSON.parse(msg.data));
        }


        this.rl = new Readline();
        this.xterm.loadAddon(this.rl);

    }

    mount(parent: Node): void {
        super.mount(parent);
        this.read();
    }

    private read () {
        this.rl.read("> ").then(data => this.send(data));
    }

    private async send (data: string) {
        let res = await this.request(data);

        if (res) {
            let message = this.pretty.print(res);
            if (res.type == "error") {
                this.logger.error(message);
            } else {
                this.logger.log(message);
            } 
        }

        if (this.mounted)
            setTimeout(() => this.read());
    }

    private request (command: string) {
        return new Promise<Packet | undefined>((res, _) => {
            this.logger.debug(">>", command);
        
            let resolved = false;
        
            this.cockpit.request(command)
                .then(packet => {
                    if (resolved) return;
                    resolved = true;
                    res(packet);
                })
                .catch((reason) => {
                    if (resolved) return;
                    resolved = true;
                    this.logger.error(`Request "${command}" failed:`, reason);
                    res(undefined);
                });
        });
    }

}