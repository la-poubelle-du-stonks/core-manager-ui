import "xterm/css/xterm.css";
import "./terminal.scss";

import { Terminal } from "xterm";
import { Widget } from "../widget";
import { FitAddon } from "xterm-addon-fit";
import { LogLine, LogLevel, Logger } from "../../cockpit/logger";

export class TerminalWidget implements Widget {

    xterm: Terminal;
    xtermFit: FitAddon;

    self: HTMLDivElement;
    observer: ResizeObserver;

    protected mounted = false;
    private colorChange: (e: MediaQueryListEvent) => void;
    private match = window.matchMedia("(prefers-color-scheme: dark)");

    constructor () {

        this.self = document.createElement("div");
        this.self.className = "terminalWidget";

        this.observer = new ResizeObserver((entries) => this.resized(entries));
        let dark = this.match.matches;

        const baseTheme = {
            background: "rgba(0,0,0,0)",
            red: "#ff0000",
            green: "#00cc25",
            yellow: "#ffe600",
            blue: "#0432ff",
            magenta: "#cc0077",
            cyan: "#0077cc"
        }

        this.xterm = new Terminal({
            theme: {
                ...baseTheme,
                black: dark ? "#6a6a6a" : "black",
                white: dark ? "white" : "#6a6a6a",
                foreground: dark ? "white" : "black"
            }
        });
        this.xterm.open(this.self);

        this.xtermFit = new FitAddon();
        this.xterm.loadAddon(this.xtermFit);

        this.colorChange = e => {
            let o = this.xterm.options;
            o.theme = {
                ...baseTheme,
                black: e.matches ? "#6a6a6a" : "black",
                white: e.matches ? "white" : "#6a6a6a",
                foreground: e.matches ? "white" : "black"
            }
        }


    }

    resized (_: ResizeObserverEntry[]) {

        // console.log(entries);
        this.xtermFit.fit();       
    }

    mount (parent: Node) {

        parent.appendChild(this.self);
        setTimeout(() => this.xtermFit.fit(), 10);

        if (this.mounted) return;
        
        this.observer.observe(this.self);
        this.mounted = true;

        this.match.addEventListener(
            "change", 
            this.colorChange
        );

    };

    unmount () {
        
        this.observer.unobserve(this.self);
        this.mounted = false;

        this.match.removeEventListener("change", this.colorChange);

    };


}

export class TerminalLogger implements Logger {

    /** The locale for the date */
    private locale: string = "fr-FR";

    /** The timezone for the date */
    private timezone: string = "Europe/Paris";

    constructor (public term: Terminal) {

    }

    log (...args: any[]) {
        this._print(this._line(LogLevel.LOG, ...args));
    }
    
    error (...args: any[]) {
        this._print(this._line(LogLevel.ERROR, ...args));
    }

    debug (...args: any[]) {
        this._print(this._line(LogLevel.DEBUG, ...args));
    }
    
    private _line (level: LogLevel, ...args: any[]): LogLine {

        let now = new Date();

        let line = {
            date: now.getTime(),
            level,
            message: ""
        }

        let msg = [];
        for (let arg of args) {

            switch (typeof arg) {

                case "string":
                    msg.push(arg);
                    break;

                case "object":
                    msg.push(JSON.stringify(arg, undefined, 4));
                    break;

                default:
                    msg.push(arg.toString());

            }

        }

        line.message = msg.join(' ');
        return line;

    }
    
    private _formatDate (timestamp: number) {
        let date = new Date(timestamp);
        return `[${date.toLocaleString(this.locale, { timeZone: this.timezone })}]`;
    }

    private _print (ln: LogLine) {
        let date = this._formatDate(ln.date);
        let deco = this.DECORATION[ln.level];
        this.term.writeln(`\x1b[1;90m${date} ${deco} ${ln.message}`);
    }

    DECORATION = {
        "INFO":     "\x1b[1;92m[INFO]\x1b[0m",
        "ERROR":    "\x1b[1;31m[ERROR]\x1b[0m",
        "WARN":     "\x1b[1;33m[WARN]\x1b[0m",
        "DEBUG":    "\x1b[1;35m[DEBUG]\x1b[0m"
    };
    
}