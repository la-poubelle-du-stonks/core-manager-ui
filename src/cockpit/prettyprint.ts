import { Packet } from "./Cockpit"
import { printError } from "./printHooks/error";
import { printHelp } from "./printHooks/help";
import { printRunnerResult, printSubprocessList } from "./printHooks/subprocess";
import { printVitals } from "./printHooks/vitals";

export type PrintHook = {
    type: string, f: (packet: Packet) => string
}

export class PrettyPrint {

    private hooks: PrintHook[] = [];

    constructor () {

        this.hook("help", printHelp);
        this.hook("error", printError);
        this.hook('logo', p => `\r\n${p.payload}`);
        this.hook("vitals", printVitals);
        this.hook("runner_result", printRunnerResult);
        this.hook("subprocess_list", printSubprocessList);

    }

    hook (type: string, f: PrintHook["f"]) {
        this.hooks.push({ type, f });
    }

    print (packet: Packet) {

        for (let h of this.hooks) {
            if (packet.type == h.type)
                return h.f(packet);
        }

        return this.basicPrint(packet);

    }

    basicPrint (packet: Packet) {
        return `(${packet.type}) ${packet.payload}`;
    }

}

