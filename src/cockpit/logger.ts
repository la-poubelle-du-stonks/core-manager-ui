export interface Logger {

    log (...args: any[]): void;
    error (...args: any[]): void;
    debug (...args: any[]): void;

}

/** Level of a log line */
export enum LogLevel {
    LOG     = "INFO",
    ERROR   = "ERROR",
    WARN    = "WARN",
    DEBUG   = "DEBUG"
}

/** A log line */
export type LogLine = {
    level: LogLevel,
    message: string,
    date: number,
};