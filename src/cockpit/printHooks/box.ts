export function niceBox (rawTitle: string, rawSections: string[][][], style = "") {

    let title = `╡ ${rawTitle} ╞`;
    let minW = title.length + 2;
    
    let sections = rawSections.map(formatSection);
    minW = sections.reduce((w, s) => Math.max(w, s[0].length), minW);
    
    let sepLine = `\r\n${style}╟${"─".repeat(minW + 2)}╢\x1b[0m\r\n`;

    let body = sections.map(
        s => s.map(
            ln => `${style}║ ${ln.padEnd(minW, " ")} ║\x1b[0m`
        ).join("\r\n")
    ).join(sepLine);
    
    let halfTitleWidth = ~~((minW + 2 - title.length) / 2);
    let topLine = `\r\n${style}╔${
        "═".repeat(halfTitleWidth + (minW - title.length) % 2)
        }${title}${
        "═".repeat(halfTitleWidth)
        }╗\x1b[0m\r\n`;
    
    let bottomLine = `\r\n${style}╚${"═".repeat(minW + 2)}╝\x1b[0m\r\n`

    return topLine + body + bottomLine;

}

function formatSection (section: string[][]): string[] {

    let colNumber = section[0].length;

    for (let i = 0; i < colNumber; i++) {
        let colWidth = section.reduce((p, s) => Math.max(p, s[i].length), 0);
        section = section.map(ln => {
            ln[i] = ln[i].padEnd(colWidth, " ");
            return ln;
        });
    }

    return section.map(ln => ln.join(" "));

}

