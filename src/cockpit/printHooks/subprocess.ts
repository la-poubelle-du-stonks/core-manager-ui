import { Packet } from "../Cockpit";
import { niceBox } from "./box";

type RunnerReturns = {
    command: { 
        exe: string, 
        args: string[], 
        workDir: string 
    },
    stdout: string,
    stderr: string
};

export function printRunnerResult (packet: Packet) {

    let result = <RunnerReturns>JSON.parse(packet.payload);
    let cmd = result.command;

    let sections: string[][][] = [];

    let commandSection = [
        [ `Command: `, `${[cmd.exe, ...cmd.args].join(' ')}` ],
        [ `Work Dir: `, cmd.workDir ]
    ];
    sections.push(commandSection)

    let stdoutSection = [
        ...result.stdout.replace("\r", "").split("\n").map(e => [e])
    ];
    sections.push(stdoutSection);

    if (result.stderr != "") {
        let stderrSection = [
            ...result.stderr.replace("\r", "").split("\n").map(e => [e])
        ];
        sections.push(stderrSection);
    }

    return niceBox("Runner Result", sections);

}


type SubprocessStatus = {
       
    id: string;
    type: 'service' | 'runner';

    exe: string;
    args: string[];
    workDir: string;
    
    created: string;
    started: string;
    ended: string;

    fails: number;
    failsSinceLastSuccess: number;
    maxFailCount: number;

    running: boolean;

}
type SubprocessList = {
    services: SubprocessStatus[],
    runners: SubprocessStatus[],
}

export function printSubprocessList (p: Packet): string {

    let result = <SubprocessList>JSON.parse(p.payload);
    let sections: string[][][] = [];

    // Runners section
    // If running:     exe args 
    // If not running: exe args [exec time] 
    let runners: string[][] = result.runners.map(r => {
        let out: string[] = [
            [r.exe, ...r.args].join(" "),
            r.running ? "" : `[${calcExecTime(r.started, r.ended)}]`
        ];

        out.unshift();
        return out;
    });

    if (runners.length)
        sections.push(runners);

    // Services section
    // If running:     [*] exe args [now - start time] 
    // If not running: [ ] exe args [end time - start time] 
    let now = (new Date()).toString();
    let services: string[][] = result.services.map(s => {
        let out: string[] = [
            s.running ? "[*]" : "[ ]",
            [s.exe, ...s.args].join(" "),
            `[${calcExecTime(s.started, s.running ? now : s.ended)}]`
        ];
        return out;
    });

    if (services.length)
        sections.push(services);


    return niceBox("Subprocess List", sections)
}

function calcExecTime (start: string, end: string) {

    let s = new Date(start), e = new Date(end);
    let delta = e.getTime() - s.getTime();
    let unit = "ms";

    if (delta >= 1000) { // Is greater than 1 second
        delta /= 1000;
        unit = "s";
    }

    if (delta >= 60) { // Is greater than a minute
        delta /= 60;
        unit = "m";
    }

    if (delta >= 60) { // Is greater than an hour
        delta /= 60;
        unit = "h";
    }
    
    if (delta >= 24) { // Is greater than a day
        delta /= 24;
        unit = "d";
    }

    return `${delta.toFixed(0)}${unit}`;

}