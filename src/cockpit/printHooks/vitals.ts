import { Packet } from "../Cockpit";
import { niceBox } from "./box";

export function printVitals (packet: Packet): string {

    let vitals = <Vitals>JSON.parse(packet.payload);
    let style = "\x1b[42m\x1b[37m";
    
    let os = [
        [ "Hostname", vitals.os.distro ],
        [ "Distro", `${vitals.os.distro} ${vitals.os.release}` ],
        [ "Kernel", vitals.os.kernel ],
    ];

    let sys = [
        [ "CPU", progress(vitals.load, 15) ],
        [ "RAM", progress(vitals.ram.use / 100, 15) ],
    ];

    let fs = vitals.fs.map(printFSLine);

    let usb = vitals.usb.map(printUSBLine);

    let net = vitals.net.map(printNetLine);
    
    let sections: string[][][] = [
        os, sys, fs, usb, net
    ];

    return niceBox("Vitals", sections, style);

}

/**
 * Returns a progressbar of lenght len
 * @param pct Percentage (0 <= pct <= 1)
 * @param len Length of the bar
 * @returns The bar
 */
function progress (pct: number, len: number): string {

    let n = Math.round(len * pct);
    let k = Math.max(len - n, 0);
    let tot = Math.round(pct * 100).toString().padStart(3, " ");

    return `[${"=".repeat(n)}${" ".repeat(k)}] ${tot}%`;

}

function printFSLine (line: Vitals["fs"][0]): string[] {

    return [ 
        `[${line.type}]`,
        line.mount,
        progress(line.use / 100, 10)
    ]

}

function printUSBLine (line: Vitals["usb"][0]): string[] {
    
    return [
        `[${line.bus}-${line.deviceId}]`,
        line.name
    ];

}

function printNetLine (line: Vitals["net"][0]): string[] {

    return [
        line.iface, line.ip4 || line.ip6 || "?"
    ]

}


type Vitals = {
    os: { hostname: string, distro: string, release: string, kernel: string },
    /** CPU load percentage */
    load: number,
    ram: { 
        /** Number of byte of the system ram */
        total: number, 
        /** Number of bytes used */
        used: number, 
        /** Percentage of ram use */
        use: number     
    },
    fs: { 
        /** Mount point of the volume */
        mount: string,
        /** Format of the volume */
        type: string,
        /** Percentage of disk use */        
        use: number,
        /** Number of bytes used */
        used: number,
        /** Total number of bytes of the volume */
        size: number
    }[],
    usb: {
        bus: number,
        deviceId: number,
        name: string
    }[],
    net: {
        /** Name of the interface */
        iface: string,
        /** IPv4 */
        ip4: string,
        /** IPv6 */
        ip6: string,
    }[]
};