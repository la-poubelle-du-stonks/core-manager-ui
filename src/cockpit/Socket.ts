import { Terminal } from "xterm";
import { CockpitClient, Packet, isPacket } from "./Cockpit";
import { Logger } from "./logger";
import { PrettyPrint } from "./prettyprint";
import { TerminalLogger } from "../widgets/terminal/terminal";
import { COCKPIT_ERR } from "./errors";

export class CockpitSocket {

    /** The WebSocket */
    public sock: WebSocket;
    
    /** The Cockpit Client */
    public client: CockpitClient;
    
    /** The logger to the main console */
    public logger: Logger;
    
    /** The pretty-printer */
    public pretty: PrettyPrint;    
    
    /** Fires when the socket is opened */
    public onopen: () => void = () => {}; 

    private readonly cartouche = "[Cockpit Socket Client]";

    constructor (
        public url: string, 
        masterTerminal: Terminal,
        id: string = ""
    ) {

        this.sock = new WebSocket(url);   
        
        this.logger = new TerminalLogger(masterTerminal);

        this.pretty = new PrettyPrint();

        this.client = new CockpitClient(
            id, 
            p => this.sendToCockpit(p),
            this.logger,
            this.pretty
        );
            
        this.bakeSocket();

    }

    /**
     * Attaches all necessary event handlers onto the socket connection
     */
    private bakeSocket () {

        this.sock.onclose = () => {
            this.logger.log(`Socket ${this.url} closed`);
        }

        this.sock.onerror = () => {
            this.client.error(
                `${this.url} can't be opened`,
                COCKPIT_ERR.socketError
            );
        }

        this.sock.onopen = async () => {
            
            this.logger.log(`Opened socket ${this.url}`);
            
            // Sends the init signals to Cockpit
            this.signal("json");
            if (this.id != "") this.signal("id", this.id);

            // Retrives the id assigned by manager while making a dummy request
            let ret = await this.client.request("logo");
            this.client.id = ret.client;

            this.onopen();

        }

        this.sock.onmessage = msg => {

            // Copy of the implementation on the manager side

            let raw: string;
            let packets: Packet[] = [];
    
            // Tries to get an utf-8 string from the recieved buffer
            try {
                raw = msg.data.toString("utf-8");
            } catch (e) {
                this.logger.error(
                    this.cartouche, 
                    "Failed to parse buffer as utf-8"
                );
                return;
            }
    
            // The messages are separated by a null character to prevent 
            // collisions, here we separate them if they collide
            // (The filter excludes any empty string using JS magic)
            let bits = raw.split("\0").filter(e => e);
    
            // We try to parse each message
            for (let b of bits) {
        
                // We will try to convert the string to JSON
                let j: Packet;                
    
                // Tries to parse the string as JSON
                try {
                    let obj = JSON.parse(b);
                    let tmp = isPacket(obj);
                    if (!tmp) {
                        this.logger.error(
                            this.cartouche,
                            "The object recieved is not a packet"
                        );
                        continue;
                    };

                    j = obj;
                    
                } catch (e) {
                    // In case of failure, an error is sent to the client                    
                    this.logger.error(
                        this.cartouche,
                        "Failed to parse message as JSON"
                    );
                    continue;
                }


                packets.push(j);
    
            }
    
            // We process each of the recieved packets
            packets.forEach(e => this.client.recieve(e));

        }

    }

    /**
     * Sends a packet over the socket
     * @param packet The packet to send
     */
    private sendToCockpit (packet: Packet) {

        let raw = JSON.stringify(packet) + "\0";

        if (this.sock.readyState != this.sock.OPEN) {
            this.client.error(
                `Trying to send messages on a closed socket (${this.url})`,
                COCKPIT_ERR.socketError
            );
            return;
        }
        
        this.sock.send(raw);
    }

    /**
     * Sends a signal with its arguments over the socket
     * @param signal The signal to send
     * @param args The signal's arguments
     */
    signal (signal: string, ...args: string[]) {

        let sig = "@" + [ signal, ...args ].join(";");

        if (this.sock.readyState != this.sock.OPEN) {
            this.client.error(
                `Trying to send messages on a closed socket (${this.url})`,
                COCKPIT_ERR.socketError
            );
            return;
        }

        this.sock.send(sig);

    }

    get id () {
        return this.client.id;
    }

    set id (id: string) {
        this.client.id = id;
        this.signal("id", id);
    }

}