import { COCKPIT_ERR, COCKPIT_ERR_MESSAGE } from "./errors";
import { Logger } from "./logger";
import { PrettyPrint } from "./prettyprint";
import { v4 } from 'uuid';

export type Packet = {
    uid: string,
    client: string,
    type: string,
    payload: string
};

export type CockpitCallback = (data: Packet) => void;
export type CockpitRequest = {
    uid: string,
    returns: CockpitCallback
}

export type CockpitHandler = {
    type: string, f: CockpitCallback
};


export class CockpitClient {

    private pending: CockpitRequest[] = [];
    private handlers: CockpitHandler[] = [];
    public timeout: number = 2000;

    /**
     * A client class for Cockpit
     * @param id ID of the client
     * @param send The function used to send packets to Core
     * @param logger The logger function
     * @param pretty The pretty printer for packets
     */
    constructor (
        public id: string,
        readonly send: CockpitCallback,
        private logger: Logger,
        private pretty: PrettyPrint
    ) {

        this.hook("error", p => {
            let txt = pretty.print(p);
            logger.error(txt);
        });
        
        this.hook("log", p => {
            logger.log(p.payload);
        });

        this.hook("log_error", p => {
            logger.error(p.payload);
        });

        this.hook("log_debug", p => {
            logger.debug(p.payload);
        });
        
    }

    /**
     * Processes the arrival of a potential packet
     * @param data The object to process
     */
    recieve (data: Object): void {

        if (!isPacket(data)) {
            // Log error
            this.error(
                JSON.stringify(data, undefined, 4), 
                COCKPIT_ERR.invalidPacket
            );
            return;
        }

        let p: Packet = <Packet>data;

        let req = this.pending.filter(e => e.uid == p.uid)[0];

        this.logger.debug(this.pretty.print(p));
        
        if (!req) {
            // Packet recieved without a request

            // Share the packet with all hooked handlers
            this.handlers.filter(e => e.type == p.type).forEach(h => h.f(p));
            return;
        }

        this.pending = this.pending.filter(r => r != req);

        req.returns(p);

    }

    /**
     * Sends a command to Core and gives a promise to wait the result
     * 
     * The promise has an integrated timeout which rejects any late response
     * 
     * @param command The command
     * @returns A promise resolving to a packet
     */
    request (command: string): Promise<Packet> {
        
        let uid = v4();
        let packet = this.packet(uid, "request", command);
        
        return new Promise((resolve, reject) => {
            
            let to = setTimeout(() => {
                this.error(
                    command,
                    COCKPIT_ERR.requestTimeout
                )
                reject("Timeout");
            }, this.timeout);

            let returns = (data: Packet) => {
                clearTimeout(to);
                resolve(data);
            }
            this.pending.push({ uid, returns });

            this.logger.debug(`Sending a request to cockpit: ${command}`);
            this.send(packet);

        });

    }

    /**
     * Sends a command to cockpit but does not expect a response
     * 
     * @param command The command to send
     * @returns The UID of the request
     */
    wildCommand (command: string): string {
        
        let uid = v4();
        let packet = this.packet(uid, "request", command);
        
        this.logger.debug(`Sending a wild command to cockpit: ${command}`);
        this.send(packet);

        return uid;

    }

    /**
     * Assembles a packet using the client's id
     * @param uid The id of the request/response 
     * @param type The type of packet
     * @param payload The payload
     * @returns The packet
     */
    packet (uid: string, type: string, payload: string): Packet {
        return {
            uid, type, payload,
            client: this.id
        }
    }

    /**
     * Adds a handler for packets of a certain type
     * @param type The type to listen to
     * @param f The function to handle the incoming Packets
     */
    hook (type: string, f: CockpitCallback) {

        this.handlers.push({ type, f });

    }

    /**
     * Removes a handler for packets with the ID "id"
     * @param f The function to remove
     */
    unhook (f: CockpitCallback) {

        this.handlers = this.handlers.filter(h => h.f != f);

    }

    
    /**
     * Generates an error message
     * 
     * @param message The text explaining the error
     * @param code The code of the error
     * @param uid The uid of the request
     */
    error (
        message: string = "", 
        code: COCKPIT_ERR = COCKPIT_ERR.unknown,
        uid?: string
    ): void {
        
        let msg = `${COCKPIT_ERR_MESSAGE[code]}${message ? ": " + message : ""}`;
        let payload = JSON.stringify({
            code, message: msg
        });
        let packet = this.packet(uid || '', "error", payload);

        this.handlers.filter(e => e.type == "error").forEach(e => e.f(packet));

    }

}

/**
 * Checks if an object is a packet
 * @param data The object to check
 * @returns true if it is a packet, else false
 */
export function isPacket (data: Object): boolean {
    return (
           (!("uid" in data) || (typeof data["uid"] == "string"))
        && ("client" in data) && (typeof data["client"] == "string")
        && ("type" in data) && (typeof data["type"] == "string")
        && ("payload" in data) && (typeof data["payload"] == "string")
    );
}
