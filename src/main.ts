import './palette.scss'
import './style.scss'
import "@fortawesome/fontawesome-free/css/all.min.css";

import { TiledLayout } from './ui/tiled/tiledLayout';
import { ShellForm } from './ui/form';
import { CockpitSocket } from './cockpit/Socket';
import { ExplorerPrototypeWidget } from './widgets/explorer/prototye';

const APP = document.querySelector<HTMLDivElement>('#app')!;

APP.innerHTML = "";

const layout = new TiledLayout();
layout.mount(APP);

let form = new ShellForm();

let ok = form.addButton("OK");
form.addTitle("Connexion");
form.addText("Veuillez renseigner l'url du Websocket pour vous connecter à Core");
let urlInput = form.addInput("core_ws_url", "URL (ws://...)");

layout.popup.open(form.self);

/*
  Screen Size change handlers 
*/
function smallScreen () {
  
}

function bigScreen () {
  
}

let isSmallScreen: boolean;
function screenResize (force?: boolean) {
  let isSmallScreenNow = (window.innerWidth / window.innerHeight) <= 1;
  if (isSmallScreenNow == isSmallScreen && !force) return;
  isSmallScreen = isSmallScreenNow;
  if (isSmallScreen) smallScreen();
  else bigScreen();
}


window.addEventListener("resize", () => {
  screenResize();
})

screenResize(true);


function connect () {

  let url = urlInput.input.value;

  let sock = new CockpitSocket(url, layout.term.xterm);
  console.log(sock)

  // let term = new SocketTerminalWidget(url);
  // layout.addWidget(term); 
  layout.popup.close();

  sock.client.hook("error", p => {

    let form = new ShellForm();
    let err = JSON.parse(p.payload);

    form.addTitle(`Error ${err.code}`);
    form.addText(err.message);
    let ok = form.addButton("OK");

    layout.popup.open(form.self);
    ok.onclick = () => layout.popup.close();

  })

  sock.onopen = () => {
    let explorer = new ExplorerPrototypeWidget(sock.client, layout);
    layout.addWidget(explorer);
  }

}

ok.onclick = () => connect();
urlInput.input.onkeyup = e => {
  if (e.key == "Enter") {
    connect()
  }
}

setTimeout(() => {

  urlInput.input.value = "ws://localhost:12002";
  connect();

})

document.addEventListener("keyup", e => {
  if (e.key == "Escape") layout.popup.close(); 
})