import "./fullscreenPopup.scss";

export class FullscreenPopup {

    self: HTMLDivElement = document.createElement("div");
    cont: HTMLDivElement = document.createElement("div");

    constructor (parent: HTMLElement) {

        this.self.className = "fullscreen-popup";

        this.cont.className = "fullscreen-popup-cont";
        this.self.append(this.cont);

        this.close();

        parent.append(this.self);

    }

    open (elem: HTMLElement) {

        let content = this.cont.querySelector("*");
        if (content) content.remove();
        
        this.cont.append(elem);
        this.self.classList.remove("hidden");

    }

    close () {

        let content = this.cont.querySelector("*");
        if (content) content.remove();

        this.self.classList.add("hidden");

    }

}