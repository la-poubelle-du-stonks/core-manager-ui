import "./tiledLayout.scss";

import { TerminalWidget } from "../../widgets/terminal/terminal"
import { Widget } from "../../widgets/widget"
import { FullscreenPopup } from "./fullscreenPopup";

export class TiledLayout implements Widget {

    self: HTMLDivElement;
    term: TerminalWidget;

    widgets: Widget[] = [];
    container: HTMLDivElement;
    private _selectedWidget: number = 0;

    navbar: HTMLDivElement;
    navbarFullscreen: HTMLButtonElement;
    navbarBack: HTMLButtonElement;
    navbarForward: HTMLButtonElement;
    navbarText: HTMLSpanElement;

    popup: FullscreenPopup;

    constructor () {

        this.self = document.createElement("div");
        this.self.className = "tiledLayout";

        this.term = new TerminalWidget();

        this.container = document.createElement("div");
        this.container.className = "tiledContainer";
        this.self.append(this.container);

        this.navbar = document.createElement("div");
        this.navbar.className = "tiledNavbar";
        this.self.append(this.navbar);

        this.navbarFullscreen = document.createElement("button");
        this.navbarFullscreen.innerHTML = `<i class="fas fa-expand"></i>`;
        this.navbar.append(this.navbarFullscreen);
        this.navbarFullscreen.onclick = () => {
            let html = document.querySelector("html");
            if (!html) return;
            if (document.fullscreenElement) {
                document.exitFullscreen();
                this.navbarFullscreen.innerHTML = `<i class="fas fa-expand"></i>`;
            } else {
                html.requestFullscreen();
                this.navbarFullscreen.innerHTML = `<i class="fas fa-compress"></i>`;
            }
        }

        this.navbarBack = document.createElement("button");
        this.navbarBack.innerHTML = `<i class="fas fa-arrow-left"></i>`;
        this.navbar.append(this.navbarBack);
        this.navbarBack.onclick = () => this.selectedWidget--;
        
        this.navbarText = document.createElement("span");
        this.navbarText.innerText = "0/0";
        this.navbar.append(this.navbarText);
        this.navbarText.onwheel = e => {
            this.selectedWidget += e.deltaY > 0 ? -1 : 1;
        };
        
        this.navbarForward = document.createElement("button");
        this.navbarForward.innerHTML = `<i class="fas fa-arrow-right"></i>`;
        this.navbar.append(this.navbarForward);        
        this.navbarForward.onclick = () => this.selectedWidget++;

        this.popup = new FullscreenPopup(this.self);

    }

    updateDisplay () {

        this.navbarText.innerText = `${this.selectedWidget + 1}/${this.widgets.length}`;
        this.container.style.setProperty("--s", this.selectedWidget.toString());

    }

    addWidget (w: Widget) {
        w.mount(this.container);

        w.self.classList.add("tiled");
        w.self.style.setProperty("--n", this.widgets.length.toString());

        this.widgets.push(w);
        this.selectedWidget = this.widgets.length - 1;
    }

    loadWidgets (ws: Widget[]) {
        for (let w of ws) {
            this.addWidget(w);
        }
    }

    closeWidget (w: Widget) {
        w.self.remove();
        w.self.classList.remove("tiled");
        w.unmount();
        let i = this.widgets.indexOf(w);
        this.widgets = this.widgets.filter(wgt => wgt != w);
        this.selectedWidget = i;
    }

    closeAllWidgets () {
        for (let w of this.widgets) {
            this.closeWidget(w);
        }
    }

    mount (parent: Node) {

        parent.appendChild(this.self);
        this.term.mount(this.self);

    };

    unmount () {

        this.self.remove(); 

    };

    get selectedWidget () {
        return this._selectedWidget;
    }

    set selectedWidget (n: number) {
        this._selectedWidget = Math.min(
            Math.max(0, n), 
            this.widgets.length - 1
        );
        this.updateDisplay();
    }

}