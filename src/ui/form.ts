import "./form.scss";

export class ShellForm {

    self: HTMLDivElement = document.createElement("div");

    body: HTMLDivElement = document.createElement("div");
    footer: HTMLDivElement = document.createElement("div");

    buttons: HTMLButtonElement[]        = [];
    titles: HTMLHeadingElement[]        = [];
    paragraphs: HTMLParagraphElement[]  = [];
    inputs: TextInput[]                 = [];

    constructor () {

        this.self.className = "shell-form";

        this.body.className = "body";
        this.self.append(this.body);
        this.body.onsubmit = e => {
            e.preventDefault();
            console.log(e)
        };
        
        this.footer.className = "footer"; // H: 60px
        this.self.append(this.footer);

    }

    /**
     * Adds a button in the footer
     * @param text Text on the button
     * @returns The button element
     */
    addButton (text: string) {

        let html = `<span>${text}</span>`;

        let btn = document.createElement("button");
        btn.innerHTML = html;

        this.buttons.push(btn);
        this.footer.append(btn);

        return btn;

    }

    /**
     * Adds a title to the body
     * @param title Text of the Title
     * @param faIcon Icon for the Title
     * @returns The title element
     */
    addTitle (title: string, faIcon?: string, color?: string) {

        let html = faIcon ? `<i class="fas fa-${faIcon}"></i>` : "";
        html = `${html}<span>${title}</span>`;

        let h = document.createElement("h1");
        h.innerHTML = html;

        if (color) h.style.setProperty("--color", color);

        this.titles.push(h);
        this.body.append(h);

        return h;

    }

    /**
     * Adds a description text to the dialog
     * @param text Text to display
     * @returns The paragraph element
     */
    addText (text: string) {

        let p = document.createElement("p");
        p.innerText = text;

        this.paragraphs.push(p);
        this.body.append(p);

        return p;

    }

    addInput (id: string, text?: string, password?: boolean) {

        let input = textInput(id, text);
        if (password) input.input.type = "password";

        this.inputs.push(input);
        this.body.append(input.wrapper);

        return input;

    }

    get minHeight () {

        const footer = 60; // 60px
        const bodyPadding = 30; // 2 * 15px
        const titleHeight = this.titles.length * 35;

        const paragraphHeight = this.paragraphs.map(p => {
            let box = p.getBoundingClientRect();
            return box.height;
        }).reduce((p, h) => p + h, 0);

        const inputHeight = this.inputs.length * 34;

        const bodyGaps = (
              this.titles.length
            + this.paragraphs.length
            + this.inputs.length
            - 1
        ) * 5;

        return footer 
             + bodyPadding 
             + bodyGaps
             + titleHeight
             + paragraphHeight
             + inputHeight;

    }

}

export type TextInput = {
    input: HTMLInputElement;
    wrapper: HTMLDivElement;
}

function textInput (id: string, placeholder?: string): TextInput {

    let input = document.createElement("input");
    let wrapper = document.createElement("div");

    wrapper.className = "text-input";
    wrapper.append(input);

    input.type = "text";
    input.id = id;
    input.placeholder = placeholder || "";

    return { wrapper, input };

}