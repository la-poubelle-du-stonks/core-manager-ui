import "./Window-aero.scss"

export class CoreWindow {

	// Properties
	_icon 		= "";
	title 		= "Window";
	x 			= 0; 
	y 			= 0;
	width 		= 360; 
	height 		= 240;
	border 		= 5; 
	minWidth 	= 240;
	minHeight 	= 64;
	topHeight	= 28;
	navbar = false;
	doubleTopBar = false;
	
	maximized 	= false;
	minimized 	= false;
	fullscreen 	= false;
	stickySides = {
		top: false, bottom: false, 
		left: false, right: false
	};
	transparent = false;
	focused 	= false;

	private _minimizable = true;
	private _resizable = true;
	
	// Status vars
	private _canResize	= false;
	private _resizing	= false;
	private _resizeDirs	= 0;		// [0-15]
	
	private _canMove	= false;
	private _moving 	= false;
	
	private _dock       = 0; // 0: None, 1: Demi-H, 2: Demi-V, 3: Quart
	private _dockSide	= 0; // bit 0: Bottom/Top, bit 1: Right/Left

	
	// HTML Elements
	self = document.createElement("div");
	
	private _bgEl 	= document.createElement("span");
	private _olEl 	= document.createElement("span");
	private _wrapEl = document.createElement("span");
	private _barEl 	= document.createElement("span");
	private _contEl = document.createElement("span");
	private _navEl 	= document.createElement("span");
	
	private _iconEl	= document.createElement("img");
	private _nameEl = document.createElement("h1");
	private _ctrlEl	= document.createElement("span");
	
	private _closeBtn = windowButton("x");
	private _maxiBtn  = windowButton("expand");
	private _miniBtn  = windowButton("window-minimize");
	private _btnEls: HTMLButtonElement[] = [];

	private _pointerId: number = 0;
	private _pointerLock: boolean = false;
	private _pointerPos: { clientX: number, clientY: number }= { clientX: 0, clientY: 0 };

	container: HTMLElement;
	contBox: DOMRect;
		
	constructor (container = document.body) {
		
		this.container = container;
		this.contBox = container.getBoundingClientRect();

		this._initLayout();	
		this._attachListeners();
		container.appendChild(this.self);
		this.renderFrame();
		
	}

	private _initLayout () {

		this.self.className = "win";
		this._bgEl.className = "bg";
		this.self.appendChild(this._bgEl);

		this._olEl.className = "outline";
		this.self.appendChild(this._olEl);

		this._wrapEl.className = "win-wrap";
		this.self.appendChild(this._wrapEl);

		this._initTitleBar();
		this._wrapEl.appendChild(this._barEl);
		
		this._navEl.className = "win-nav";
		this._wrapEl.appendChild(this._navEl);

		this._contEl.className = "cont";
		this._wrapEl.appendChild(this._contEl);
	}
	
	private _initTitleBar() {
		this._barEl.className = "win-bar";
		
		if (this._icon) {
			this._barEl.appendChild(this._iconEl);
			this._iconEl.src = this._icon;
		} else {
			this._iconEl.remove();
		}
		
		this._nameEl.innerText = this.title;
		this._barEl.appendChild(this._nameEl);
			
		this._ctrlEl.className = "controls";
		this._barEl.appendChild(this._ctrlEl);
		
		for (let btn of this._btnEls) {
			btn.remove();
		}

		this._btnEls = [];
		this._btnEls.push(this._closeBtn);
		
		if (this._resizable) {
			this._btnEls.push(this._maxiBtn);
		}

		if (this._minimizable) {
			this._btnEls.push(this._miniBtn);
		}

		for (let btn of this._btnEls) {
			btn.className = "btn";
			this._ctrlEl.appendChild(btn);
		}

	}
	
	private _attachListeners () {
		
		
		this.self.addEventListener("pointerleave", () => {
			this._canMove = false;
			this._canResize = false;
		});
		
		this.self.addEventListener("pointermove", e => this._selfPointerMove(e));
		this.self.addEventListener("pointerdown", e => {
			this._selfPointerMove(e);

			this._pointerLock = true;
			this._pointerId = e.pointerId;
			this._pointerPos.clientX = e.clientX;
			this._pointerPos.clientY = e.clientY;
		});

		this._nameEl.addEventListener("dblclick", () => {
			this.maximized = !this.maximized;
		});

		document.addEventListener("pointermove", e => {
			
			if (e.pointerId != this._pointerId) return;
			
			let mx = e.clientX - this._pointerPos.clientX;
			let my = e.clientY - this._pointerPos.clientY;

			this._pointerPos.clientX = e.clientX;
			this._pointerPos.clientY = e.clientY;

			if (this._moving) {
				this.x += mx;
				this.y += my;
				this._dock = 0;
				this._dockSide = 0;
			}

			if (this._resizing) {
				if (this._resizeDirs & 8) {
					this.y += my;
					this.height -= my;
				}
				else if (this._resizeDirs & 4) {
					this.height += my;
				}

				if (this._resizeDirs & 2) {
					this.x += mx;
					this.width -= mx;
				}
				else if (this._resizeDirs & 1) {
					this.width += mx;
				}
			}

		});

		document.addEventListener("pointerdown", e => {
					
			if (e.pointerId != this._pointerId) return;

			if (this._canMove) {
				this._moving = true;
			}
			else if (this._canResize) {
				this._resizing = true;
			}
			
		});

		document.addEventListener("pointerup", e => this._pointerUp(e));
		document.addEventListener("pointerleave", e => this._pointerUp(e));

		this._btnEls[0].onclick = () => console.log("Close Window");
		this._btnEls[1].onclick = () => this.maximized = !this.maximized;
		this._btnEls[2].onclick = () => this.minimized = !this.minimized;

	}

	private _pointerUp (e: PointerEvent) {
		if (e.pointerId != this._pointerId) return;
		this._pointerLock = false;

		if (this._moving) {
			this._moving = false;
			this.snap();
		}
		if (this._resizing) {
			this._resizing = false;
			this.snap();
		}
	}

	private _selfPointerMove (e: PointerEvent) {

		if (this._pointerLock) return;
		this._pointerId = e.pointerId;

		let ox = e.offsetX;
		let oy = e.offsetY;

		if (!this._resizing) {
			let box = this.self.getBoundingClientRect();
			this._resizeDirs =  
				(oy < this.border && 1 || 0) << 3 |
				(oy >= - this.border + box.height && 1 || 0) << 2 |
				(ox < this.border && 1 || 0) << 1 |
				(ox >= - this.border + box.width && 1 || 0) << 0;
		}

		this._canResize = !!this._resizeDirs && !this.maximized && (e.target == this._nameEl || e.target == this.self) && !this.transparent && !this._dock && this._resizable;

		this._canMove = e.target == this._nameEl && !this._canResize && !this.maximized;

	}

	public snap () {
		// Calculs intermédiares
		let box = this.self.getBoundingClientRect();
		let dx = box.width - this.width;
		let dy = box.height - this.height;
		
		// Calculs côtés de docking
		let top = this.y <= -25;
		let bottom = this.y >= this.contBox.height - 0.5 * box.height;
		let left = this.x <= - 0.5 * box.width;
		let right = this.x >= this.contBox.width - 0.5 * box.width;

		// Vérification des tailles minimales
		let minW = this.minWidth + 2 * this.border <= this.contBox.width / 2;
		let minH = this.minHeight + (this.doubleTopBar ? 2 : 1) * this.border + this.topHeight <= this.contBox.height / 2;

		top &&= minH && this._resizable;
		bottom &&= minH && this._resizable;
		left &&= minW && this._resizable;
		right&&= minW && this._resizable;

		// Correction de la position
		this.width = clamp(this.width, this.minWidth, this.contBox.width - dx);
		this.height = clamp(this.height, this.minHeight, this.contBox.height - dy);
		this.x = clamp(this.x, 0, this.contBox.width - 2 * this.border - this.width);		
		this.y = clamp(this.y, 0, this.contBox.height - this.topHeight - this.height - this.border);

		// Docking
		this._dock |= ( (top || bottom) && 1 || 0 );
		this._dock |= ( (left || right) && 2 || 0 );
		this._dock &= ( minH && 3 || 2 ); 
		this._dock &= ( minW && 3 || 1 ); 

		this._dockSide |= top && 1 || 0;
		this._dockSide |= left && 2 || 0;
		
		if (this._dock) {
			this.x = right ? this.contBox.width / 2 : 0;
			this.y = bottom ? this.contBox.height / 2 : 0;
		}
		
	}

	/**
	 * Centers the window relative to its container
	 */
	center () {

		this.x = this.contBox.width / 2 - this.width / 2 - this.border;
		this.y = this.contBox.height / 2 - this.height / 2 - this.topHeight / 2 - this.border * (this.doubleTopBar ? 1 : 0.5);
		this.snap();

	}
	
	setApp (app: Node) {
		this._contEl.appendChild(app);
	}
	
	renderFrame () {
		this.contBox = this.container.getBoundingClientRect();
		this.self.style.setProperty('--x', `${this.x}`);
		this.self.style.setProperty('--y', `${this.y}`);
		this.self.style.setProperty('--w', `${this.width}px`);
		this.self.style.setProperty('--h', `${this.height}px`);
		this.self.style.setProperty('--minw', `${this.minWidth}px`);
		this.self.style.setProperty('--minh', `${this.minHeight}px`);
		this.self.style.setProperty('--p', `${this.border}px`);
		this.self.style.setProperty('--top-height', `${this.topHeight}px`);
		this.self.style.setProperty('--nav-enabled', `${this.navbar && 1 || 0}`);
		this.self.classList.toggle("dblTopBar", this.doubleTopBar);
		this.self.classList.toggle("maxi", this.maximized);
		this.self.classList.toggle("mini", this.minimized);
		this.self.classList.toggle("fullscreen", this.fullscreen);
		this.self.classList.toggle("resize", this._canResize || this._resizing);
		let dirInt = this._resizeDirs;
		this.self.classList.toggle("w", dirInt == 1 || dirInt == 2);
		this.self.classList.toggle("h", dirInt == 4 || dirInt == 8);
		this.self.classList.toggle("nesw", dirInt == 5 || dirInt == 10);
		this.self.classList.toggle("nwse", dirInt == 9 || dirInt == 6);  
		this.self.classList.toggle("inhibit", this._moving || this._resizing);
		this._nameEl.innerText = this.title;
		this.self.classList.toggle("demi-h", this._dock == 1);
		this.self.classList.toggle("demi-v", this._dock == 2);
		this.self.classList.toggle("quart", this._dock == 3);
		if (this._dock == 1) {
			this.self.classList.toggle("top", !!(this._dockSide & 1));
			this.self.classList.toggle("bottom", !(this._dockSide & 1));
		}
		if (this._dock == 2) {
			this.self.classList.toggle("left", !!(this._dockSide & 2));
			this.self.classList.toggle("right", !(this._dockSide & 2));
		}
		if (this._dock == 3) {
			this.self.classList.toggle("se", this._dockSide == 0);
			this.self.classList.toggle("ne", this._dockSide == 1);
			this.self.classList.toggle("sw", this._dockSide == 2);
			this.self.classList.toggle("nw", this._dockSide == 3);
		}
		this.self.classList.toggle("transparent", this.transparent);
	}

	set icon (icon) {
		
		this._icon = icon;
		this._initTitleBar();
		
	}

	get icon () {
		return this._icon;
	}

	get resizable () {
		return this._resizable;
	}

	set resizable (r: boolean) {
		this._resizable = r;
		this._initTitleBar();
	}
	
	get minimizable () {
		return this._minimizable;
	}

	set minimizable (r: boolean) {
		this._minimizable = r;
		this._initTitleBar();
	}

	

}

function windowButton (btn: string) {
	let b = document.createElement("button")
	b.innerHTML = `<i class="fas fa-${btn}"></i>`;
	return b;
}

function clamp (value: number, min: number, max: number) {
	return Math.max(min, Math.min(max, value));
}